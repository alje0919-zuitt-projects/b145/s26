const http = require('http');

const port = 4000;

// create a varilable "server" that stores the output of the createServer method
const server = http.createServer((req, res) => {

	// to access the greeting (routes) we will use the request object
	// https://localhost:4000/greeting
	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Hello Again');
	} else if(req.url == '/homepage') {
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end('This is the Homepage, Welcome!');
	} else if(req.url == '/about') {
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end('Welcome to About page');
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Page not found');
	}

})

// user the server and port variable
server.listen(port);

// when server is running, print this:
console.log(`Server now accessible at localhost: ${port}.`);

//============================================================
