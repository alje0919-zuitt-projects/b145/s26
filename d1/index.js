// Create a basic server application by loading NODE Js modules

// MODULE - it is a software component or part of a program that contains one or more routines.

// http -> Hyper Text Transfer Protocol
// HTTP is a module to node that transfer data. a set of files that contain code to create a component that helps establish data transfer between applications
// Clients (browser) and servers (node js/express) communicate by exchanging individual messages.
// The messages sent by the client, usually a web browser, are called REQUEST.
// The message sent by the server as an answer are called RESPONSE.
let http = require("http");


// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the listen(4000) method where the server will listen to any requests that are sent to our server.
// http://localhost:4000
http.createServer(function(request, response) {

	// user the writeHead method to
	// set a status code for the response - 200 means OK
	// 
	response.writeHead(200, {'Content-Type': 'application/json'});
	response.end("Hello World");


}).listen(4000);

// When server is running, console will print the message:
console.log('Server running at localhost:4000');

// Status code
// 100-199 Informational responses
// 200-299 Successful responses
// 300-399 Redirection messages
// 400-499 Client error
// 500-599 Server error responses
