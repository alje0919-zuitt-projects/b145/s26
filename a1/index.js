/*
7. Create a server using the createServer method that will listen in to the port provided above.
8. Console log in the terminal a message when the server is successfully running.
9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.

11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.
13. Create a git repository named S26.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s26 Activity"
15. Add the link in Boodle. "Node Js Introduction"
*/

const http = require('http');
const port = 3000;

// create a varilable "server" that stores the output of the createServer method
const server = http.createServer((req, res) => {

	// to access the greeting (routes) we will use the request object
	// https://localhost:4000/greeting
	if(req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to the Login page.');
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Error: The page you\'re trying to access can\'t be found.');
	}

})

// user the server and port variable
server.listen(port);

// when server is running, print this:
console.log(`Server is currently up and running: ${port}.`);